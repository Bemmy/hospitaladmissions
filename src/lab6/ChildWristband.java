/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

import java.util.*;

public class ChildWristband extends Wristband {
    
    private List<Parent> parents;
    
    public ChildWristband(List<Parent> parents){
        this.parents = parents;
    }
    
    public List<Parent> getTotalParents(){
        return parents;
    }
    
}
