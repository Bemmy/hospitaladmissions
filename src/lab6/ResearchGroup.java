/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

import java.util.*;

public class ResearchGroup {
    
    List<Patient> patients;
    
    public ResearchGroup(List<Patient> patients){
        this.patients = patients;
    }
    
    public List<Patient> getTotalPatients(){
        return patients;
    }
}
