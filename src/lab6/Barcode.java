/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

public class Barcode {
    
    protected int barcode;
    
    public Barcode(){}
    
    public Barcode(int barcode){
        this.barcode = barcode;
    }
    
    public int getBarcode(){
        return barcode;
    }
    
    public void setBarcode(int barcode){
        this.barcode = barcode;
    }
}