
package lab6;

import java.util.*;

public class Simulation {
    
    public static void main(String[] args){
        
        Doctor doctor = new Doctor("Dr. Berns");
        Barcode code = new Barcode(29380495);
        Medication med = new Medication("Advil");
        List<Medication> meds = new ArrayList<Medication>();
        meds.add(med);
        String patientName = "Sam Rowles";
        Wristband band = new AllergyWristband(patientName, new Date(), doctor, code, meds);
        
        List<Wristband> bands = new ArrayList<Wristband>();
        bands.add(band);
        
        Patient patient = new Patient(patientName, bands);
        
        List<Patient> patients = new ArrayList<Patient>();
        patients.add(patient);
        
        ResearchGroup group = new ResearchGroup(patients);
   
    }
}
