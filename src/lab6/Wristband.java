/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

import java.util.Date;

public class Wristband extends Barcode {
    
    protected String name;
    protected Doctor doc;
    protected Date dob;
    protected Barcode barcode;
    
    public Wristband(){}
    
    public Wristband(Barcode barcode, String name, Doctor doc, Date dob){
        
        this.barcode = barcode;
        this.name = name;
        this.doc = doc;
        this.dob = dob;
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public Doctor getDoc(){
        return doc;
    }
    
    public void setDoct(Doctor doc){
        this.doc = doc;
    }
    
    public Date getDob(){
       return dob;
    }
    
    public void setDob(Date dob){
        this.dob = dob;
    }
    
    public Barcode getTotalBarcodes(){
        return barcode;
    }
    
    public void setTotalBarcode(Barcode barcode){
        this.barcode= barcode;
    }
    
}
