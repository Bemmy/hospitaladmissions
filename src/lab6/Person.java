/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

public class Person {
    
    protected String name;
    
    protected Person(){}
    
    protected Person(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }   
}
