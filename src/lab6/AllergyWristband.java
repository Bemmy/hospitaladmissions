/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

import java.util.*;

public class AllergyWristband extends Wristband {
    
    private List<Medication> medications;

    public AllergyWristband(List<Medication> medications){
        this.medications = medications;
    }
    
    public AllergyWristband(String patientName, Date dob, Doctor doc, Barcode barcode, List<Medication> medications){
        this.name = patientName;
        this.dob = dob;
        this.doc = doc;
        this.barcode = barcode;
        this.medications = medications;
        
    }
    
    public List<Medication> getTotalMedications(){
        return medications;
    }
    
    public Doctor getTotalDoctors(){
        return doc;
    }
    
    public Barcode getTotalBarcodes(){
        return barcode;
    }
    
    public Date getTotalDob(){
        return dob;
    }
}
