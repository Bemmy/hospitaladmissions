/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

import java.util.*;

public class Patient extends Person {
    
    private List<Wristband> bands;
    
    public Patient(String name){
        super(name);
    }
    
    public Patient(String name, List<Wristband> bands){
        super(name);
        this.bands = bands;
    }
    
    public List<Wristband> getTotalBands(){
        return bands;
    }
}
